<?php
/**
 * Template Name: Custom Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/page', 'header'); ?>

	
	<!--Search Bar-->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3" style="float:right;">
	            <div class="input-group" id="adv-search">
	                <input type="text" class="form-control" placeholder="Search for results" />
	                <div class="input-group-btn">
	                    <div class="btn-group" role="group">
	                        <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
	                    </div>
	                </div>
	            </div>
	          </div>
	        </div>
		</div>
	</div>
	<!--/Search Bar-->

	<!--Image Header-->
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="lg-header-image">
			</div>
		</div>
	</div>
	<!--/Image Header-->


	<?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
