<?php
/**
 * Template Name: Contact Page Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/page', 'header'); ?>

	<!--Search Bar-->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3" style="float:right;">
	            <div class="input-group" id="adv-search">
	                <input type="text" class="form-control" placeholder="Search for results" />
	                <div class="input-group-btn">
	                    <div class="btn-group" role="group">
	                        <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
	                    </div>
	                </div>
	            </div>
	          </div>
	        </div>
		</div>
	</div>
	<!--/Search Bar-->

	<!--/Google Map Header-->
	<div class="col-lg-12">
		<div class="container-fluid">
			<div id="map-canvas"></div>
		</div>
	</div>
	<!--/Google Map Header-->


	<!--Contact Form-->
	<div class="col-lg-12">
		<div class="container-fluid">
			<form>
				<h3>If you have any questions, please contact me</h3>
				<div class="form-group">
					<label for="exampleInputEmail1">Email address</label>
					<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">Password</label>
					<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
				</div>
				<div class="form-group">
					<label for="exampleInputFile">File input</label>
					<input type="file" id="exampleInputFile">
					<p class="help-block">Upload a PDF or Word Document</p>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox"> Check this if your message is urdgent
					</label>
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
	</div>
	<!--/Contact Form-->


	<?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
