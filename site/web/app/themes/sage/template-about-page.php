<?php
/**
 * Template Name: About Page Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/page', 'header'); ?>

	<!--Search Bar-->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3" style="float:right;">
	            <div class="input-group" id="adv-search">
	                <input type="text" class="form-control" placeholder="Search for results" />
	                <div class="input-group-btn">
	                    <div class="btn-group" role="group">
	                        <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
	                    </div>
	                </div>
	            </div>
	          </div>
	        </div>
		</div>
	</div>
	<!--/Search Bar-->


	<!--Basic Info 1-->
	<div class="container-fluid">
		<div class="col-lg-12">
			About information 1
		</div>
	</div>
	<!--/Basic Info 1-->

	<!--Basic Info 2-->
	<div class="container-fluid">
		<div class="col-lg-12">
			About Information 2
		</div>
	</div>
	<!--/Basic Info 2-->

	<!--Basic Info 3-->
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="col-lg-4">
				About Information 3
			</div>
				<div class="col-lg-4">
				About Information 3
			</div>
				<div class="col-lg-4">
				About Information 3
			</div>
		</div>
	</div>
	<!--/Basic Info 3-->



	<?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
