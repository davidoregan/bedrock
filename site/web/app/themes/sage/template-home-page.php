<?php
/**
 * Template Name: Single Page Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/page', 'header'); ?>

	<!--Image Header-->
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="lg-header-image">
			</div>
		</div>
	</div>
	<!--/Image Header-->


	<!--Search Bar-->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3" style="float:right;">
	            <div class="input-group" id="adv-search">
	                <input type="text" class="form-control" placeholder="Search for results" />
	                <div class="input-group-btn">
	                    <div class="btn-group" role="group">
	                        <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
	                    </div>
	                </div>
	            </div>
	          </div>
	        </div>
		</div>
	</div>
	<!--/Search Bar-->

	<!--Table With Data-->
	<div class="container-fluid">
		<div class="row">


			<div class="col-md-12">
				<h4>Results</h4>
				<div class="table-responsive">


					<table id="mytable" class="table table-bordred table-striped">

						<thead>

							<th><input type="checkbox" id="checkall" /></th>
							<th>Make</th>
							<th>Model</th>
							<th>Asking Price</th>
							<th>Predicted Price</th>
							<th>Good Deal?</th>
							<th>Edit</th>
							<th>Delete</th>
						</thead>
						<tbody>

							<tr>
								<td><input type="checkbox" class="checkthis" /></td>
								<td>BMW</td>
								<td>M3</td>
								<td>27,000 Euro</td>
								<td>23,000 Euro</td>
								<td>No</td>
								<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
								<td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
							</tr>

							<tr>
								<td><input type="checkbox" class="checkthis" /></td>
								<td>Ford</td>
								<td>Focus</td>
								<td>7,670 Euro</td>
								<td>8,120 Euro</td>
								<td>Yes!</td>
								<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
								<td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
							</tr>


							<tr>
								<td><input type="checkbox" class="checkthis" /></td>
								<td>Honda</td>
								<td>Civic</td>
								<td>3,240 Euro</td>
								<td>3,150 Euro</td>
								<td>No</td>
								<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
								<td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
							</tr>



							<tr>
								<td><input type="checkbox" class="checkthis" /></td>
								<td>Nissian</td>
								<td>Skyline</td>
								<td>8,900 Euro</td>
								<td>9,540 Euro</td>
								<td>Yes!</td>
								<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
								<td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
							</tr>

							<tr>
								<td><input type="checkbox" class="checkthis" /></td>
								<td>Hyundai</td>
								<td>X-Series</td>
								<td>12,370 Euro</td>
								<td>9,975 Euro</td>
								<td>No</td>
								<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
								<td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
							</tr>

							<tr>
								<td><input type="checkbox" class="checkthis" /></td>
								<td>Nissian</td>
								<td>GTR</td>
								<td>27,950 Euro</td>
								<td>33,425 Euro</td>
								<td>Yes!</td>
								<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
								<td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
							</tr>

						</tbody>
					</table>

					<div class="clearfix"></div>
					<ul class="pagination pull-right">
						<li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
						<li class="active"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
					</ul>

				</div>

			</div>
		</div>
	</div>


	<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
					<h4 class="modal-title custom_align" id="Heading">Edit Your Detail</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input class="form-control " type="text" placeholder="Make">
					</div>
					<div class="form-group">
						<input class="form-control " type="text" placeholder="Model">
					</div>
					<div class="form-group">
						<textarea rows="2" class="form-control" placeholder="Details"></textarea>
					</div>
				</div>
				<div class="modal-footer ">
					<button type="button" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
				</div>
			</div>
			<!-- /.modal-content --> 
		</div>
		<!-- /.modal-dialog --> 
	</div>



	<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
					<h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
				</div>
				<div class="modal-footer ">
					<button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
				</div>
			</div>
			<!-- /.modal-content --> 
		</div>
		<!-- /.modal-dialog --> 
	</div>
	<!--/Table with Data-->

	<!--Chart Graph-->
	<div class="container-fluid">
	<h2>Charts</h2>
		<div class="col-md-4">
			<h5>Linear Regression Line Chart</h5>
			<div id="canvas-holder">
				<canvas id="C" height="300" width="300"></canvas>
			</div>
		</div>
		<div class="col-md-4">
			<h5>Prediction Model</h5>
			<div id="canvas-holder">
				<canvas id="A" width="300" height="300"/><br>
			</div>
		</div>

		<div class="col-md-4">
			<h5>Support Vector Machine</h5>
			<div id="canvas-holder">
				<canvas id="B" width="300" height="300"/><br>
			</div>
		</div>
	</div>
	<!--/Chart Graph-->


	<!--Gallery-->
	<div class="col-lg-12">
		<div class="container-fluid">
		<h2>Gallery</h2>
			<div class="row">
				<div class="col-md-3"><img class="img-responsive" src="http://eecs.wsu.edu/~cook/ml/logo_mlc.jpg" /></div>
		        <div class="col-md-3"><img class="img-responsive" src="http://www.hdwallpapersjpg.com/wp-content/uploads/2015/03/Stylish-Car-8488-Nice-HD-Wallpaper.jpg" /></div>
		        <div class="col-md-3"><img class="img-responsive" src="http://wallpoper.com/images/00/28/91/00/datatech-blue_00289100.png" /></div>
		        <div class="col-md-3"><img class="img-responsive" src="https://ratherrad.files.wordpress.com/2012/04/funny-desktop-wallpapers-thechive-14.jpg" /></div>
		    </div>
			<div class="row">
				<div class="col-md-3"><img class="img-responsive" src="http://eecs.wsu.edu/~cook/ml/logo_mlc.jpg" /></div>
		        <div class="col-md-3"><img class="img-responsive" src="http://www.hdwallpapersjpg.com/wp-content/uploads/2015/03/Stylish-Car-8488-Nice-HD-Wallpaper.jpg" /></div>
		        <div class="col-md-3"><img class="img-responsive" src="http://wallpoper.com/images/00/28/91/00/datatech-blue_00289100.png" /></div>
		        <div class="col-md-3"><img class="img-responsive" src="https://ratherrad.files.wordpress.com/2012/04/funny-desktop-wallpapers-thechive-14.jpg" /></div>
		    </div>
		</div>
	</div>
	<!--/Gallery-->

	<?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
